# XMPP chat bot
XMPP chat bot

## Install dependencies on Debian-based systems

sudo apt install python3-slixmpp

## Install dependencies with pip
pip install -r requirements.txt

## Setup
Create `config.ini` based on the `config.ini.example`, with your credentials
