import ollama
import scraper_functions as sf
import random
from frontends import FRONTENDS

def processmsg(msg, rcpt):
    if msg.startswith("!"):
        return command(msg, "")
    elif "good bot" in msg:
        return "^_^"
    for big_tech_site in FRONTENDS:
        if big_tech_site in msg:
            return "libre link: " + msg.replace(big_tech_site, random.choice(FRONTENDS[big_tech_site]))

def command(msg, rcpt):
    if msg.startswith("!help"):
        response = "chatbot commands:"  + "\n"
        response += "!help            Show this help page" + "\n"
        response += "!ai [message]    Ask llama2" + "\n"
        response += "!wiki [message]    Ask wiki\n"
        response += "!tasks           Show active tasks from the taskmanager\n"
        response += "!vreme [city] | !prognoza [city] | !weather [city]    Show weather for [city]\n"
        return response
    elif msg.startswith("!ai"):
        client = ollama.Client(host='https://ollama.krov.dmz.rs')
        response = client.chat(model='llama2-uncensored:latest', messages=[{'role':'user','content':f'{msg[4:]}'}])
        return(response['message']['content'])
    elif msg.startswith("!wiki"):
        cmd, query = msg.split(" ", 1)
        return sf.query_external_website("https://en.wikipedia.org", "/wiki/" + query)
    elif msg.startswith("!tasks"):
        content = sf.getDmzTasks("https://todo.dmz.rs/")
        return content
    elif msg.startswith("!vreme") or msg.startswith("!prognoza") or msg.startswith("!weather"):
        commandsplit = msg.split(" ", 1)
        if len(commandsplit) == 1:
            return sf.get_weather("Beograd")
        else:
            query = commandsplit[1]
            return sf.get_weather(query)
