FRONTENDS = {
	"www.youtube.com/watch": ("iv.datura.network/watch", "inv.tux.pizza/watch"),
	"youtube.com/watch": ("iv.datura.network/watch", "inv.tux.pizza/watch"),
	"medium.com" : ("scribe.rip", "sc.vern.cc", "m.opnxng.com"),
	"stackoverflow.com": ("code.whatever.social", "ao.vern.cc", "overflow.smnz.de"),
	"instagram.com": ("bibliogram.1d4.us", "bibliogram.froth.zone", "ig.opnxng.com", "proxigram.lunar.icu"),
	"genius.com": ("dm.vern.cc", "dumb.lunar.icu", "dumb.esmailelbob.xyz"),
	"reddit.com":("eu.safereddit.com", "l.opnxng.com", "libreddit.bus-hit.me"),
	"www.imdb.com": ("libremdb.iket.me", "ld.vern.cc", "binge.whatever.social"),
	"twitter.com": ("n.opnxng.com", "nitter.1d4.us", "nitter.adminforge.de"),
	"wikipedia.com": ("wiki.adminforge.de", "wiki.froth.zone", "wikiless.esmailelbob.xyz")
}
